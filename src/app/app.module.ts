import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { CookieService } from 'ngx-cookie-service';
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';
import { HttpLoaderFactory, MissingTranslationService } from '@share/services/httpLoaderFactory';

import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { SidebarModule } from 'primeng/sidebar';
import { MenuModule } from 'primeng/menu';
import { MenubarModule } from 'primeng/menubar';
import { AccordionModule } from 'primeng/accordion';
import { ChartModule } from 'primeng/chart';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { CalendarModule } from 'primeng/calendar';
import { TabViewModule } from 'primeng/tabview';
import { InputTextModule } from 'primeng/inputtext';
import { ContextMenuModule } from 'primeng/contextmenu';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { DialogModule } from 'primeng/dialog';
import { FileUploadModule } from 'primeng/fileupload';
import { TooltipModule } from 'primeng/tooltip';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LoginService } from '@services/login.service';
import { UserSettingsService } from '@services/user-settings.service';
import { HttpApiService } from '@share/services/http-api.service';
import { CookiesService } from '@share/services/cookies.service';


import { SnippetModule } from '@modules/snippet/snippet.module';
import { HolidayBannerModule } from '@modules/holiday-banner/holiday-banner.module';
import { AuthGuard } from '@guards/auth.guard';

import { LoginPageComponent } from '@components/login-page/login-page.component';
import { HomePageComponent } from '@components/home-page/home-page.component';
import { ReesterPageComponent } from '@components/reester-page/reester-page.component';
import { TopMenuComponent } from '@components/top-menu/top-menu.component';
import { Grid01Component } from '@components/grid01/grid01.component';
import { Grid02Component } from '@components/grid02/grid02.component';
import { UsersGridComponent } from './components/users-grid/users-grid.component';
import { UnknownGridComponent } from './components/unknown-grid/unknown-grid.component';
import { SimpleCardComponent } from './components/simple-card/simple-card.component';
import { SimpleCardSecondComponent } from './components/simple-card-second/simple-card-second.component';
import { ShortFormComponent } from './components/short-form/short-form.component';
import { CanDeactivateGuard } from '@share/guards/leave.guard';
import { ExampleTablePageComponent } from './components/example-table-page/example-table-page.component';
import { DirectivesModule } from '@share/directives/directives.module';
import { ExampleTablePageV2Component } from './components/example-table-page-v2/example-table-page-v2.component';

// import { environment } from 'src/environments/environment';

// export class MissingTranslationService implements MissingTranslationHandler {
//   handle(params: MissingTranslationHandlerParams) {
//     if (!environment.production) {
//       console.log(`WARN: '${params.key}' is missing in '${params.translateService.currentLang}' locale`)
//     }
//     return params.key;
//   }
// }

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    HomePageComponent,
    ReesterPageComponent,
    TopMenuComponent,
    Grid01Component,
    Grid02Component,
    UsersGridComponent,
    UnknownGridComponent,
    SimpleCardComponent,
    SimpleCardSecondComponent,
    ShortFormComponent,
    ExampleTablePageComponent,
    ExampleTablePageV2Component,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useClass: MissingTranslationService
      },
      useDefaultLang: false
    }),
    FormsModule,
    TableModule,
    ButtonModule,
    ToastModule,
    SidebarModule,
    MenuModule,
    MenubarModule,
    AccordionModule,
    ChartModule,
    RadioButtonModule,
    DropdownModule,
    CheckboxModule,
    CalendarModule,
    TabViewModule,
    InputTextModule,
    ContextMenuModule,
    ToggleButtonModule,
    DialogModule,
    FileUploadModule,
    TooltipModule,
    AppRoutingModule,
    SnippetModule,
    HolidayBannerModule,
    DirectivesModule
  ],
  providers: [
    LoginService,
    HttpApiService,
    UserSettingsService,
    AuthGuard,
    CanDeactivateGuard,
    MessageService,
    CookieService,
    CookiesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
