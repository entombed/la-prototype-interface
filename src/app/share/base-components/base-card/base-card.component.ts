import { OnInit, Output, EventEmitter, ElementRef, Renderer2, AfterViewInit, Input } from '@angular/core';
// import { AnimationEvent } from '@angular/animations';
import { MessageService } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { of, Observable } from 'rxjs';

export interface CardFlags {
  editingModeOff: boolean;
  options?: object;
}

export class BaseCardComponent implements OnInit, AfterViewInit {

  @Input() additive = ''; // примесь для формирования уникальных ID в DOM
  // @Output() closeCard: EventEmitter<any> = new EventEmitter<any>();

  // public animatoState = 'visible'; // указываем состояние анимации карточки
  public flags: CardFlags = {
    editingModeOff: true, // карточка в режиме редактирования или нет
  };
  public statusCollapse = false;
  // protected bodyElement: HTMLBodyElement; // сохраняем body
  // protected countModalWindows: number; // кол-во открытых модальных окон в body
  public showIsSaved = false;
  public showCloseWindow = false; // окно предупреждение при закрытии карточки когда она в режиме редактирования
  protected i18nWords: string[] = []; // массив слов для которых необходимо получить перевод
  public translations: {} = {}; // словарик с переведенными словами
  constructor(
    protected elementRef: ElementRef,
    protected renderer: Renderer2,
    protected messageService: MessageService,
    protected translateService: TranslateService,
  ) {
  }

  ngOnInit() {
  }

  /**
   * сохраням в переменную body
   * подсчитываем кол-во открытых карточек в момент создания компонента
   *
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @memberof BaseCardComponent
   */
  ngAfterViewInit() {
    // window.addEventListener('beforeunload', (event) => {
    //   event.preventDefault();
    //   return event;
    // });
  }

  // /**
  //  * отлавливаем нажатие ESC
  //  * смотрим у какого компонента на странице кол-во карточек === 1
  //  * считаем что это самая последняя открытая карточка, запускаем закрытие этой карточки
  //  *
  //  * @author A.Bondarenko
  //  * @date 2020-01-27
  //  * @param {KeyboardEvent} event
  //  * @memberof BaseCardComponent
  //  */
  // @HostListener('document:keydown.Escape', ['$event'])
  // onEscapePress(event: KeyboardEvent) {
  //   const modal = this.elementRef.nativeElement.querySelectorAll('.be-modal-card-overlay').length;
  //   if (modal === 1) {
  //     this.startCloseCard();
  //   }
  // }

  /**
   *
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @protected
   * @param {string[]} [arrayWords=this.i18nWords] - массив слов которые необходимо перевести
   * @param {(() => void)} [callBack] - callback функция
   * @memberof BaseCardComponent
   */
  protected getTranslates(arrayWords: string[] = this.i18nWords, callBack?: (() => void)): void {
    this.translateService.get(arrayWords).subscribe((translatedWords) => {
      this.translations = translatedWords;
      if (callBack) {
        callBack();
      }
    });
  }

  protected getTranslatesRxJs(arrayWords: string[] = this.i18nWords): Observable<any> {
    return this.translateService.get(arrayWords).pipe(
      map((translatedWords) => {
        this.translations = translatedWords;
        return translatedWords;
      })
    );
  }

  // /**
  //  * отключаем скрол на body
  //  * для того что бы предотвратить появления второго скрола
  //  *
  //  * @author A.Bondarenko
  //  * @date 2020-01-27
  //  * @private
  //  * @memberof BaseCardComponent
  //  */
  // private disableBodyScroll() {
  //   if (this.bodyElement.style.overflow !== 'hidden') {
  //     this.renderer.setStyle(this.bodyElement, 'overflow', 'hidden');
  //   }
  // }

  // /**
  //  * закрываем карточку,
  //  * если это будет последняя открытая карточка
  //  * возвращяем body скрол
  //  *
  //  * @author A.Bondarenko
  //  * @date 2020-01-27
  //  * @param {*} [event]
  //  * @memberof BaseCardComponent
  //  */
  // public setCardClosed(event?) {
  //   if (this.countModalWindows === 1) {
  //     this.renderer.removeStyle(this.bodyElement, 'overflow');
  //   }
  //   this.closeCard.emit({ closed: true });
  // }

  // /**
  //  * при закрытии карточки смотрим,
  //  * если она в режиме редактирования выводим окно предупреждение
  //  * если она не в режиме редактирования запускаем анимацию
  //  *
  //  * @author A.Bondarenko
  //  * @date 2020-01-27
  //  * @memberof BaseCardComponent
  //  */
  // startCloseCard() {
  //   if (this.flags.editingModeOff) {
  //     this.changeAnimationState('close');
  //   } else {
  //     this.showCloseWindow = true;
  //   }
  // }

  // /**
  //  * изменяем состояния анимации
  //  *
  //  * @author A.Bondarenko
  //  * @date 2020-01-27
  //  * @param {*} state
  //  * @memberof BaseCardComponent
  //  */
  // public changeAnimationState(state) {
  //   this.animatoState = state;
  // }

  /**
   * обрабатываем нажатие кнопки сохранить
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @memberof BaseCardComponent
   */
  public saveCard() {
    console.log('save operation');
    this.showInfoMsg('Information successfully saved');
  }

  /**
   * запрашиваем по новой данные карточки с сервера
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @memberof BaseCardComponent
   */
  public renewCard() {
    console.log('renewCard operation');
    this.showInfoMsg('Information successfully refreshed');
  }

  /**
   * закрываем все открытые акардионы на странице
   * выбираем все открытыте акардионы и закрываем их
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @memberof BaseCardComponent
   */
  // public collapseOpennedAccordions(event) {
  //   const listAccordion = this.elementRef.nativeElement.querySelectorAll('.ui-accordion-header.ui-state-active > a');
  //   listAccordion.forEach((element: HTMLElement) => {
  //     this.renderer.selectRootElement(element, true).click();
  //   });
  // }
  public collapseOpennedAccordions(event) {
    const setCollapse = event.checked;
    let listAccordion = null;
    if (setCollapse) {
      listAccordion = this.elementRef.nativeElement.querySelectorAll('.ui-accordion-header.ui-state-active > a');
    } else {
      listAccordion = this.elementRef.nativeElement.querySelectorAll('.ui-accordion-header:not(.ui-state-active) > a');
    }
    listAccordion.forEach((element: HTMLElement) => {
      this.renderer.selectRootElement(element, true).click();
    });
  }

  /**
   * делаем поля формы доступными для редактирования или нет
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @param {*} mode
   * @memberof BaseCardComponent
   */
  public setLockFields(mode) {
    this.flags.editingModeOff = mode;
  }

  // /**
  //  * запускаем после завершения анимации
  //  * закрытие карточки
  //  *
  //  * @author A.Bondarenko
  //  * @date 2020-01-27
  //  * @param {*} event
  //  * @memberof BaseCardComponent
  //  */
  // public animationDone(event: AnimationEvent) {
  //   if (event.toState === 'close') {
  //     this.setCardClosed(event);
  //   }
  // }

  /**
   * информационное сообщение
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @protected
   * @param {string} message
   * @memberof BaseCardComponent
   */
  protected showInfoMsg(message: string): void {
    this.messageService.add(
      { severity: 'info', summary: 'Information Message', detail: message }
    );
  }

  /**
   * сообщение об ошибке
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @protected
   * @param {string} message
   * @memberof BaseCardComponent
   */
  protected showErrorMsg(message: string): void {
    this.messageService.add(
      { severity: 'error', summary: 'Something went wrong', detail: message, life: 3000 }
    );
  }

}
