import { OnInit, ElementRef, Renderer2, AfterViewInit, HostListener, Output, EventEmitter, OnDestroy } from '@angular/core';
import { BaseCardComponent } from '../base-card/base-card.component';
import { MessageService } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { AnimationEvent } from '@angular/animations';


export class ModalBaseCardComponent extends BaseCardComponent implements OnInit, AfterViewInit, OnDestroy {

  @Output() closeCard: EventEmitter<any> = new EventEmitter<any>();

  public animatoState = 'visible'; // указываем состояние анимации карточки
  protected bodyElement: HTMLBodyElement; // сохраняем body
  protected countModalWindows: number; // кол-во открытых модальных окон в body
  public isStandalonePage = false; // открыта карточка в самостоятельном окне браузера или нет
  constructor(
    protected elementRef: ElementRef,
    protected renderer: Renderer2,
    protected messageService: MessageService,
    protected translateService: TranslateService,
  ) {
    super(elementRef, renderer, messageService, translateService);

  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.countModalWindows === 1) {
      this.renderer.removeStyle(this.bodyElement, 'overflow');
    }
  }

  ngAfterViewInit() {
    this.bodyElement = this.renderer.selectRootElement('body', true);
    this.countModalWindows = this.bodyElement.querySelectorAll('.be-modal-card-overlay').length;
    this.disableBodyScroll();
  }



  /**
   * отлавливаем нажатие ESC
   * смотрим у какого компонента на странице кол-во карточек === 1
   * считаем что это самая последняя открытая карточка, запускаем закрытие этой карточки
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @param {KeyboardEvent} event
   * @memberof BaseCardComponent
   */
  @HostListener('document:keydown.Escape', ['$event'])
  onEscapePress(event: KeyboardEvent) {
    const modal = this.elementRef.nativeElement.querySelectorAll('.be-modal-card-overlay').length;
    if (modal === 1) {
      this.startCloseCard();
    }
  }

  // @HostListener('window:pagehide', ['$event'])
  // beforeunloadHandler(event) {
  //   console.log('By refreshing this page you may lost all data.');
  // }
  // @HostListener('document:beforeunload', ['$event'])
  // onListenetEvent(event) {
  //   console.log(event)
  // }


  /**
   * отключаем скрол на body
   * для того что бы предотвратить появления второго скрола
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @private
   * @memberof BaseCardComponent
   */
  private disableBodyScroll() {
    if (this.bodyElement.style.overflow !== 'hidden') {
      this.renderer.setStyle(this.bodyElement, 'overflow', 'hidden');
    }
  }

  /**
   * закрываем карточку,
   * если это будет последняя открытая карточка
   * возвращяем body скрол
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @param {*} [event]
   * @memberof BaseCardComponent
   */
  public setCardClosed(event?) {
    // if (this.countModalWindows === 1) {
    //   this.renderer.removeStyle(this.bodyElement, 'overflow');
    // }
    this.closeCard.emit({ closed: true });
  }

  /**
   * при закрытии карточки смотрим,
   * если она в режиме редактирования выводим окно предупреждение
   * если она не в режиме редактирования запускаем анимацию
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @memberof BaseCardComponent
   */
  startCloseCard() {
    if (this.flags.editingModeOff) {
      this.changeAnimationState('close');
    } else {
      this.showCloseWindow = true;
    }
  }

  /**
   * изменяем состояния анимации
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @param {*} state
   * @memberof BaseCardComponent
   */
  public changeAnimationState(state) {
    this.animatoState = state;
  }

  /**
   * запускаем после завершения анимации
   * закрытие карточки
   *
   * @author A.Bondarenko
   * @date 2020-01-27
   * @param {*} event
   * @memberof BaseCardComponent
   */
  public animationDone(event: AnimationEvent) {
    if (event.toState === 'close') {
      this.setCardClosed(event);
    }
  }

}
