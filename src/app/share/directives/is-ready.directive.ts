import { Directive, AfterViewInit, Output, EventEmitter, ElementRef } from '@angular/core';

@Directive({
  selector: '[appIsReady]'
})
export class IsReadyDirective implements AfterViewInit {
  @Output() appIsReady: EventEmitter<any> = new EventEmitter();
  constructor(public el: ElementRef) { }
  ngAfterViewInit() {
      this.appIsReady.emit(this.el);
  }
}


