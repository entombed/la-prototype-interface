import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IsReadyDirective } from './is-ready.directive';



@NgModule({
  declarations: [
    IsReadyDirective,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    IsReadyDirective
  ]
})
export class DirectivesModule { }
