import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SnippetBlockComponent } from './snippet-block/snippet-block.component';
import { LoadingSnippetService } from './loading-snippet.service';
import { LoadingSnippetInterceptorService } from './loading-snippet-interceptor.service';


@NgModule({
  declarations: [
    SnippetBlockComponent
  ],
  imports: [
    CommonModule,
  ],
  providers: [
    LoadingSnippetService,
    // { provide: HTTP_INTERCEPTORS, useClass: LoadingSnippetInterceptorService, multi: true },
  ],
  exports: [
    SnippetBlockComponent
  ]
})
export class SnippetModule { }
