import { Injectable } from '@angular/core';

export type HolidayClasses = 'default-banner' | 'new-year-banner' | 'spring-banner';

@Injectable({
  providedIn: 'root'
})
export class HolidayBannerService {

  private holidayName: HolidayClasses;
  constructor() { }

  public setHolidayName(name: HolidayClasses = 'default-banner') {
    this.holidayName = name;
  }

  public getHolidayName() {
    return this.holidayName;
  }
}
