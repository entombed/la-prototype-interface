import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LoadingSnippetService } from '@share/modules/snippet/loading-snippet.service';

@Injectable({
  providedIn: 'root'
})
export class LoadingSnippetInterceptorService implements HttpInterceptor {
  constructor(
    public loadingSnippetService: LoadingSnippetService
  ) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loadingSnippetService.show(); // даем сигнал что нужно отображать сниппет в начале http запроса
    return next.handle(req).pipe(
      finalize(() => {
        this.loadingSnippetService.hide(); // прячем сниппет когда http запрос завершен
      })
    );
  }
}
