import { Injectable } from '@angular/core';
import { LoadingSnippetService } from '@modules/snippet/loading-snippet.service';
import { HolidayBannerService, HolidayClasses } from '@modules/holiday-banner/holiday-banner.service';
import { UserSettingsService } from './user-settings.service';
import { CookiesService } from './cookies.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(
    private loadingSnippetService: LoadingSnippetService,
    private holidayBannerService: HolidayBannerService,
    private userSettingsService: UserSettingsService,
    private cookiesService: CookiesService
  ) { }

  public isAuthenticated(login = '', password = '') {
    this.loadingSnippetService.show(); // запускаем отображение рагрузки
    return new Promise(
      (resolve, reject) => {
        const token = this.cookiesService.getToken();
        if (token) {
          this.loadingSnippetService.hide();
          this.userSettingsService.setIsLoggIn(true);
          resolve(this.userSettingsService.getIsLoggIn());
        } else {
          setTimeout(
            () => {
              // this.holidayBannerService.setHolidayName('default-banner'); // указываем какой баннер отображать
              if (login === 'q' && password === 'q') {
                this.cookiesService.setCookie();
                this.userSettingsService.setIsLoggIn(true);
              } else {
                this.userSettingsService.setIsLoggIn(false);
              }
              this.loadingSnippetService.hide(); // прячем отображение рагрузки
              resolve(this.userSettingsService.getIsLoggIn());
            },
            1000
          );
        }
      }
    );
  }

  public login() {
    // this.loggedIn = true;
  }

  public logout() {
    this.cookiesService.deleteAllCookie();
    this.userSettingsService.setIsLoggIn(false);
  }

  public checkIsHasAccess() {
    const token = this.cookiesService.getToken();
    if (token) {
      console.log(token);
      this.cookiesService.updateCookie();
      this.userSettingsService.setIsLoggIn(true);
    } else {
      this.userSettingsService.setIsLoggIn(false);
    }
    return new Promise((resolve, reject) => {
      resolve( this.userSettingsService.getIsLoggIn() );
    });
  }
}
