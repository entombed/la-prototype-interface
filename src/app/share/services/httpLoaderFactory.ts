import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MissingTranslationHandlerParams, MissingTranslationHandler } from '@ngx-translate/core';
import { environment } from '@environments/environment';

export function HttpLoaderFactory(httpClient: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json');
}

/**
 * отлавливаем когда нет перевода
 * выводим сообщение в консоль только в режиме разработки
 *
 * @author A.Bondarenko
 * @date 2020-01-28
 * @export
 * @class MissingTranslationService
 * @implements {MissingTranslationHandler}
 */
export class MissingTranslationService implements MissingTranslationHandler {
  handle(params: MissingTranslationHandlerParams) {
    if (environment.development) {
      // console.warn(`WARN: Key '${params.key}' is missing in ${params.translateService.currentLang}.json file`)
    }
    return params.key;
  }
}
