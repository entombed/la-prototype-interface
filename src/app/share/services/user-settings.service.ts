import { Injectable } from '@angular/core';

export type Language = 'ru' | 'en';

@Injectable({
  providedIn: 'root'
})

export class UserSettingsService {
  private lang: Language = 'ru';
  private isLoggIn = false;

  constructor() { }

  public getLang(): Language {
    return this.lang;
  }

  public setLang(lang: Language): void {
    this.lang = lang;
  }

  public getIsLoggIn(): boolean {
    return this.isLoggIn;
  }
  public setIsLoggIn(flag: boolean): void {
    this.isLoggIn = flag;
  }
}
