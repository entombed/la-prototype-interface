import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpApiService {
  private baseUrl = 'https://reqres.in/api';
  constructor(
    private httpClient: HttpClient
  ) { }

  public getUsersPage(numPage = 1): any {
    return this.httpClient.get(`${this.baseUrl}/users?page=${numPage}`);
  }

  public getUnknown(numPage = 1): any {
    return this.httpClient.get(`${this.baseUrl}/unknown?page=${numPage}`);
  }

  public getSingleUser(id): any {
    return this.httpClient.get(`${this.baseUrl}/users/${id}`);
  }

}
