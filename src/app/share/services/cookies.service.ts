import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root',
})
export class CookiesService {

  constructor(
    private cookieService: CookieService
  ) { }

  public setCookie(nameCookie = 'accessToken') {
    const date = new Date(Date.now() + 1800000);
    this.cookieService.set(nameCookie, `${Date.now()}`, date, '/', null, false, 'Strict' );
  }

  public updateCookie(nameCookie = 'accessToken', time = 1800000) {
    if (this.cookieService.check(nameCookie)) {
      const date = new Date(Date.now() + time);
      this.cookieService.set(nameCookie, `${Date.now()}`, date, '/', null, false, 'Strict' );
    }
  }

  public getToken(nameCookie = 'accessToken') {
    return this.cookieService.get(nameCookie);
  }

  public deleteAllCookie() {
    this.cookieService.deleteAll();
  }
}
