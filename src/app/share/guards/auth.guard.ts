import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '@services/login.service';
import { MessageService } from 'primeng/api';
import { UserSettingsService } from '@share/services/user-settings.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private loginService: LoginService,
    private router: Router,
    private userSettingsService: UserSettingsService,
    private messageService: MessageService
  ) {

  }
  // canActivate(
  //   next: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot
  // ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  //   return this.loginService.isAuthenticated()
  //     .then((data: boolean) => {
  //       if (data) {
  //         return data;
  //       } else {
  //         this.router.navigate(['/']);
  //         this.messageService.add(
  //           { severity: 'info', summary: 'Message of Guard', detail: 'Access denied! Try again' }
  //         );
  //         return false;
  //       }
  //     })
  //     .catch((error) => {
  //       this.router.navigate(['/']);
  //       console.log('route error', error);
  //       return false;
  //     });
  // }

  // canActivate(
  //   next: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot
  // ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  //   return new Promise((resolve, reject) => {
  //     const token = this.loginService.getToken();
  //     if (token) {
  //       console.log(token)
  //       resolve(true);
  //     }
  //     if (this.userSettingsService.getIsLoggIn()) {
  //       resolve(true);
  //     } else {
  //       this.router.navigate(['/']);
  //       resolve(false);
  //     }
  //   });
  // }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.loginService.checkIsHasAccess()
      .then((data: boolean) => {
        if (data) {
          return data;
        } else {
          this.router.navigate(['/']);
          this.messageService.add(
            { severity: 'info', summary: 'Message of Guard', detail: 'Access denied! Try again' }
          );
          return false;
        }
      })
      .catch((error) => {
        this.router.navigate(['/']);
        console.log('route error', error);
        return false;
      });
  }

}
