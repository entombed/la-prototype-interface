import { Component, HostListener, AfterViewInit, Inject, OnDestroy, OnInit } from '@angular/core';
import { environment } from '@environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { UserSettingsService } from '@share/services/user-settings.service';
import { fromEvent } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { CookiesService } from '@share/services/cookies.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {

  public showContextMenu = false;
  private targetContextMenu: any;
  private isDevelopmentMode: boolean; // работаем в режиме разработки или продакшен
  public contextMenu = [
    {
      label: 'Get Component Name',
      command: () => { this.showNameOfApp(); }
    }
  ];

  constructor(
    private translateService: TranslateService,
    private userSettingsService: UserSettingsService,
    private activateRoute: ActivatedRoute,
    private cookiesService: CookiesService,
    @Inject(DOCUMENT) doc: Document
  ) {
    this.userSettingsService.setLang('ru'); // сохраняем в сервисе язык приложения
    const lang = this.userSettingsService.getLang(); // забираем язык из сервиса
    this.translateService.use(lang); // говорим какой язык использовать для переводов
    this.isDevelopmentMode = environment.development; // проверяем в каком режиме работаем
    // this.activateRoute.url.subscribe((data) => console.log(data))
    // console.log(this.activateRoute.snapshot)

  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // if (this.isDevelopmentMode) {
    //   window.addEventListener('beforeunload', (event) => {
    //     // console.log(event)
    //     event.preventDefault();
    //     event.returnValue = 'Unsaved modifications';
    //     return event;
    //   });
    // }
    // this.preventRefreshPage();
  }
  ngOnDestroy() {
    console.log('Destroy app');
    this.cookiesService.deleteAllCookie();
  }

  private preventRefreshPage() {
    fromEvent(window, 'beforeunload').subscribe((event) => {
      event.preventDefault();
      return event;
    });
  }


  /**
   * отлавливаем нажатие Ctrl
   * разрешаем вызывать свое контестное меню только при условии:
   *    работаем в режиме разработки
   *    нажат Ctrl
   *
   * @author A.Bondarenko
   * @date 2020-01-14
   * @param {*} event
   * @memberof AppComponent
   */
  @HostListener('document:keydown.Control', ['$event'])
  onCtrlPress(event: KeyboardEvent) {
    if (this.isDevelopmentMode) {
      this.showContextMenu = true;
    }
  }

  /**
   * ловим событие когда пользователь хочет рефрешнуть страницу
   *
   * @param {BeforeUnloadEvent} $event
   * @returns
   * @memberof AppComponent
   */
  // @HostListener('window:beforeunload', ['$event'])
  // onReload($event: BeforeUnloadEvent) {
  //   return $event.returnValue = 'Your data will be lost!';
  // }

  /**
   * захватываем DOM елемент на котором вызвали контекстное меню
   * при условии что работаем в режиме разработки
   *
   * @author A.Bondarenko
   * @date 2020-01-14
   * @param {MouseEvent} event
   * @memberof AppComponent
   */
  @HostListener('window:contextmenu', ['$event'])
  onContextMenu(event: MouseEvent): void {
    if (this.isDevelopmentMode) {
      this.targetContextMenu = event.target;
    }
  }

  /**
   * выводим в консоль имя родительского angular компонента
   * на котором вызвали контекстное меню
   *
   * @author A.Bondarenko
   * @date 2020-01-14
   * @private
   * @memberof AppComponent
   */
  private showNameOfApp() {
    let domElement: HTMLElement = this.targetContextMenu;
    let i = 0;
    while (domElement && ('parentElement' in domElement)) {
      const localName = domElement.localName.toLowerCase();
      if (/^app-\w+/i.test(localName)) {
        console.log(localName);
        break;
      }
      domElement = domElement.parentElement;
      i++;
    }
  }
}
