import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from '@components/login-page/login-page.component';
import { HomePageComponent } from '@components/home-page/home-page.component';
import { AuthGuard } from '@guards/auth.guard';
import { ReesterPageComponent } from '@components/reester-page/reester-page.component';
import { Grid01Component } from '@components/grid01/grid01.component';
import { Grid02Component } from '@components/grid02/grid02.component';
import { ShortFormComponent } from '@components/short-form/short-form.component';
import { CanDeactivateGuard } from '@share/guards/leave.guard';
import { SimpleCardComponent } from '@components/simple-card/simple-card.component';
import { ExampleTablePageComponent } from '@components/example-table-page/example-table-page.component';
import { ExampleTablePageV2Component } from '@components/example-table-page-v2/example-table-page-v2.component';

const routes: Routes = [
  { path: '', component: LoginPageComponent },
  { path: 'Operations', component: HomePageComponent, canActivate: [AuthGuard], children: [
    { path: '', component: ShortFormComponent, canActivate: [AuthGuard] },
    { path: 'GridOperations01', component: Grid01Component, canActivate: [AuthGuard], canDeactivate: [CanDeactivateGuard] },
    { path: 'GridOperations02', component: Grid02Component, canDeactivate: [CanDeactivateGuard] },
    { path: 'ShortForm', component: ShortFormComponent, canActivate: [AuthGuard], canDeactivate: [CanDeactivateGuard] },
    { path: 'ExampleTable', component: ExampleTablePageComponent, canActivate: [AuthGuard] },
    { path: 'ExampleTableV2', component: ExampleTablePageV2Component, canActivate: [AuthGuard] }
  ] },
  { path: 'Reester', component: ReesterPageComponent, canActivate: [AuthGuard], children: [
    { path: 'Grid01', component: Grid01Component, canDeactivate: [CanDeactivateGuard] },
    { path: 'Grid02', component: Grid02Component, canDeactivate: [CanDeactivateGuard] },
  ] },
  { path: 'simple/:id', component: SimpleCardComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
