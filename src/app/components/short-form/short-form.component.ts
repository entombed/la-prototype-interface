import { Component, OnInit, ElementRef, Renderer2, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseCardComponent } from '@share/base-components/base-card/base-card.component';
import { MessageService } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { CanComponentDeactivate } from '@share/guards/leave.guard';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-short-form',
  templateUrl: './short-form.component.html',
  styleUrls: ['./short-form.component.less'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShortFormComponent extends BaseCardComponent implements OnInit, CanComponentDeactivate {
  additive = 'QWERTY'
  showSimpleCard = false;
  contactPhoneCode = null;
  jobType = null;
  realty = null;
  productModel: string = null;

  checkBoxTable = [
    {
      name: 'CHECK_1',
      items: [
        {
          value1: true,
          value2: true,
          value3: false,
          value4: false,
        },
        {
          value1: true,
          value2: false,
          value3: true,
          value4: true,
        }
      ]
    },
    {
      name: 'CHECK_2',
      items: [
        {
          value1: true,
          value2: true,
          value3: false,
          value4: false,
        },
        {
          value1: true,
          value2: false,
          value3: true,
          value4: false,
        },
        {
          value1: false,
          value2: false,
          value3: true,
          value4: true,
        }
      ]
    }
  ];

  checkBoxFilds = [
    {name: 'Загальна інформація', lenght: '2', fild: 'value1'},
    {name: 'Відділення', lenght: '2', fild: 'value2'},
    {name: 'ПіБ ініціатора заяви', lenght: '2', fild: 'value3'},
    {name: 'Дата і час останнього оновлення', lenght: '2', fild: 'value4'},
  ];

  productType = [
    { label: null, value: null },
    { label: 'Тип продукту 1', value: '1' },
    { label: 'Тип продукту 2', value: '2' },
    { label: 'Тип продукту 3', value: '3' },
    { label: 'Тип продукту 4', value: '4' },
    { label: 'Тип продукту 5', value: '5' },
    { label: 'Тип продукту 6', value: '6' },
  ];
  citizenship = [
    { label: null, value: null },
    { label: 'Грамодянство 1', value: '1' },
    { label: 'Грамодянство 2', value: '2' },
    { label: 'Грамодянство 3', value: '3' },
    { label: 'Грамодянство 4', value: '4' },
    { label: 'Грамодянство 5', value: '5' },
    { label: 'Грамодянство 6', value: '6' },
  ];
  sex = [
    { label: null, value: null },
    { label: 'Чоловіча', value: 'M' },
    { label: 'Жіноча', value: 'W' },
  ];
  personalDocument = [
    { label: null, value: null },
    { label: 'Персональний документ 1', value: '1' },
    { label: 'Персональний документ 2', value: '2' },
    { label: 'Персональний документ 3', value: '3' },
  ];
  maritalStatus = [
    { label: null, value: null },
    { label: 'У шлюбі', value: '1' },
    { label: 'Розлучений(-на)', value: '2' },
    { label: 'Неодружений(-на)', value: '3' },
  ];
  educationLevel = [
    { label: null, value: null },
    { label: 'Середнє', value: '1' },
    { label: 'Вища', value: '2' }
  ];

  area = [
    { label: null, value: null },
    { label: 'Київ', value: null },
    { label: 'Харків', value: null },
    { label: 'Одеса', value: null },
    { label: 'Херсон', value: null }
  ]
  population = [
    { label: null, value: null },
    { label: '> 10 тис чоловік', value: 10 },
    { label: '> 20 тис чоловік', value: 20 },
    { label: '> 30 тис чоловік', value: 30 },
    { label: '> 40 тис чоловік', value: 40 },
    { label: '> 50 тис чоловік', value: 50 },
    { label: '> 60 тис чоловік', value: 60 },
    { label: '> 70 тис чоловік', value: 70 },
    { label: '> 80 тис чоловік', value: 80 },
    { label: '> 90 тис чоловік', value: 90 },
    { label: '> 100 тис чоловік', value: 100 },
    { label: '> 200 тис чоловік', value: 200 },
    { label: '> 300 тис чоловік', value: 300 },
    { label: '> 400 тис чоловік', value: 400 },
  ];

  lived = [
    { label: null, value: null },
    { label: 'Від 1 року', value: 1 },
    { label: 'Від 5 року', value: 2 },
    { label: 'Від 10 року', value: 3 },
    { label: 'Від 15 року', value: 4 },
  ];

  i18nWords = ['SAVED_MSG', 'CLOSE_MSG'];
  listJobs = [
    {label: 'Оптова торгівля', value: '1'},
    {label: 'Туристичні агентства', value: '2'},
    {label: 'Страхові агентства', value: '3'},
    {label: 'Виробництво харчових продуктів', value: '4'},
    {label: 'Виробництво меблів', value: '5'},
    {label: 'Виробництво деревини та виробів з деревини', value: '6'},
    {label: 'Поліграфічні послуги', value: '7'},
    {label: 'Міський та приміський пасажирський транспорт', value: '8'},
    {label: 'Рослинництво і тваринництво', value: '9'},
    {label: 'Виробництво одягу', value: '10'},
    {label: 'Виробництво ювелір.виробів/біжутерії/інших виробів ', value: '11'},
    {label: 'Роздрібна торгівля', value: '12'},
    {label: 'Автомобільні вантажні перевезення', value: '13'},
    {label: 'Будівництво будівель', value: '14'},
    {label: 'Послуги оренди та лізингу', value: '15'},
    {label: 'Послуги з розміщення (готелі)', value: '16'},
    {label: 'Фотографічні послуги', value: '17'},
    {label: 'Видобуток каменю, піску та глини', value: '18'},
    {label: 'Різання, обробка та оздоблення каменю', value: '19'},
    {label: 'Надання послуг перукарнями та салонами краси', value: '20'},
    {label: 'Організація поховань та пов`язаних з ними послуг', value: '21'},
  ];

  constructor(
    protected elementRef: ElementRef,
    protected renderer: Renderer2,
    protected messageService: MessageService,
    protected translateService: TranslateService,
    private cookieService: CookieService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    super(elementRef, renderer, messageService, translateService);
  }

  ngOnInit() {
    this.getTranslatesRxJs().subscribe((data) => {
      console.log(data);
    });
  }

  onBasicUpload(event) {
    console.log(event);
  }

  closeCard(event) {
    this.showSimpleCard = false;
  }

  checkCheckBox(event, rowIndex, itemRowIndex, fild) {
    console.log(rowIndex, itemRowIndex, fild)
    console.log(this.checkBoxTable)
  }

  showCard() {
    // this.changeDetectorRef.markForCheck();
    this.showSimpleCard = true;
  }

  canDeactivate(): boolean | Observable<boolean> {
    console.log('!!!!!!!!!!!!!!!!')
    if (!this.flags.editingModeOff) {
      return window.confirm('Страница в режиме редактирования. Вы хотите покинуть страницу?');
    } else {
      return true;
    }
  }

  getTooltip(dictionary, code) {
    console.log(dictionary)
    console.log(code)
  }
}
