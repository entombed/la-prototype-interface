import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';
import { HttpApiService } from '@share/services/http-api.service';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { CanComponentDeactivate } from '@share/guards/leave.guard';

@Component({
  selector: 'app-grid02',
  templateUrl: './grid02.component.html',
  styleUrls: ['./grid02.component.less']
})
export class Grid02Component implements OnInit, CanComponentDeactivate {
  ifFullScreen = false;
  cols: any[] = [
    { field: 'email', header: 'EMAIL' },
    { field: 'first_name', header: 'FIRST_NAME' },
    { field: 'last_name', header: 'LAST_NAME' },
    { field: 'id', header: 'ID' },
  ];

  dataLineChart = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'First Dataset',
        data: [65, 59, 80, 81, 56, 55, 40],
        fill: false,
        borderColor: '#4bc0c0'
      },
      {
        label: 'Second Dataset',
        data: [28, 48, 40, 19, 86, 27, 90],
        fill: false,
        borderColor: '#565656'
      }
    ]
  };
  public dataGrid: object[];
  public rowsCount = 2;
  public first = 0;
  constructor(
    private messageService: MessageService,
    private httpApiService: HttpApiService,
    private renderer: Renderer2,
    // private elementRef: ElementRef
  ) { }

  ngOnInit() {
    this.getData();
  }

  private getData() {
    this.httpApiService.getUsersPage().subscribe(
      (data) => {
        this.dataGrid = data.data;
      },
      (error) => { console.log(error); }
    );
  }
  selectData(event) {
    this.messageService.add({
      severity: 'info',
      summary: 'Data Selected',
      detail: `${this.dataLineChart.datasets[event.element._datasetIndex].data[event.element._index]}`
    });
  }

  public rowSelect(event) {

  }

  public clickToggleButton(event) {
    this.showNameOfApp(event.originalEvent.target, event.checked)
  }

  private showNameOfApp(event: any, checked: boolean) {
    let domElement = event;
    let i = 0;
    const bodyElement = this.renderer.selectRootElement('body', true);
    while (domElement && ('parentElement' in domElement)) {
      const className = domElement.className.split(' ');
      if (className.includes('ui-accordion-content')) {
        break;
      }
      domElement = domElement.parentElement;
      i++;
    }
    if (checked) {
      this.renderer.setStyle(bodyElement, 'overflow-y', 'hidden');
      this.renderer.addClass(domElement, 'be-full-screen-wrapper');
      this.rowsCount = 10;
    } else {
      this.renderer.removeStyle(bodyElement, 'overflow-y');
      this.renderer.removeClass(domElement, 'be-full-screen-wrapper');
      this.rowsCount = 2;
    }
  }

  canDeactivate(): boolean | Observable<boolean> {
    return confirm('Вы хотите покинуть страницу?');
  }

}
