import { Component, OnInit, Renderer2, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { HolidayBannerService } from '@share/modules/holiday-banner/holiday-banner.service';
import { LoginService } from '@share/services/login.service';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-reester-page',
  templateUrl: './reester-page.component.html',
  styleUrls: ['./reester-page.component.less']
})
export class ReesterPageComponent implements OnInit {
  public display = true;
  public itemMenu: MenuItem[];
  public showBanner = true;

  constructor(
    private holidayBannerService: HolidayBannerService,
    private router: Router,
    private renderer: Renderer2,
    private elementRef: ElementRef,
    private loginService: LoginService
  ) {
    this.holidayBannerService.setHolidayName('default-banner');
  }

  ngOnInit() {
    // this.holidayBannerService.setHolidayName('default-banner');
    this.itemMenu = [{
      label: 'Grids',
      id: '1',
      items: [
        { id: '11', label: 'Grid01', command: (data) => { this.goto(data); }  },
        { id: '12', label: 'Grid02', command: (data) => { this.goto(data); }  },
      ]
    }];
  }

    /**
   * отслеживаем скрол окна
   * прячем баннер если болше 400px проскроли вверх
   * показываем если скрол меньше 300px
   *
   * @author A.Bondarenko
   * @date 2020-01-14
   * @param {*} event
   * @memberof HomePageComponent
   */
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event) {
    // console.log(event.target.documentElement.scrollTop);
    const pageYOffset = event.target.documentElement.scrollTop;
    if (pageYOffset > 400) {
      this.showBanner = false;
    } else if (pageYOffset < 300) {
      this.showBanner = true;
    }
  }

  public goto(data) {
    this.router.navigate([`/Reester/${data.item.label}`]).then((data) => {
      this.display = false;
    });
  }

    /**
   * сворациваем все открытые акардио панель
   *
   * @author A.Bondarenko
   * @date 2020-01-14
   * @private
   * @memberof HomePageComponent
   */
  public collapseOpennedAccordions(event) {
    const listAccordion = this.elementRef.nativeElement.querySelectorAll('.ui-accordion-header.ui-state-active > a');
    listAccordion.forEach((element: HTMLElement) => {
      this.renderer.selectRootElement(element, true).click();
    });
  }

  /**
   * кнопака выхода из приложения
   *
   * @author A.Bondarenko
   * @date 2020-01-14
   * @param {*} event
   * @memberof HomePageComponent
   */
  public logout(event) {
    this.loginService.logout();
    this.router.navigate(['/']);
  }

}
