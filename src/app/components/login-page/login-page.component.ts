import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '@services/login.service';
import { CookiesService } from '@share/services/cookies.service';


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.less'],
})
export class LoginPageComponent implements OnInit {
  public showProgressBar = false;
  public userLogin: string = null;
  public userPassword: string = null;
  element: any;
  constructor(
    private router: Router,
    private activateRoute: ActivatedRoute,
    private loginService: LoginService,
    private renderer: Renderer2,
    private cookiesService: CookiesService,
  ) {
  }

  ngOnInit() {
  }

  public handleClick(event) {
    this.loginService.isAuthenticated(this.userLogin, this.userPassword).then(() => {
      this.router.navigate(['/Operations']).then((data: boolean) => {
        // this.cookiesService.setCookie();
      });
    });
  }

}
