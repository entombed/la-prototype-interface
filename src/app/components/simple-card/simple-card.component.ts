import { Component, OnInit, HostListener, Output, EventEmitter, ElementRef, Renderer2, AfterViewInit, Input } from '@angular/core';
import { MessageService } from 'primeng/api';
// import { BaseCardComponent } from '@share/base-components/base-card/base-card.component';
import { AnimatiModalCard } from '@share/animations/card-animation';
import { TranslateService } from '@ngx-translate/core';
import { ModalBaseCardComponent } from '@share/base-components/modal-base-card/modal-base-card.component';
import { ActivatedRoute, Router, Params, ParamMap } from '@angular/router';
import { HttpApiService } from '@share/services/http-api.service';
import { map, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';


@Component({
  selector: 'app-simple-card',
  templateUrl: './simple-card.component.html',
  styleUrls: ['./simple-card.component.less'],
  animations: [AnimatiModalCard]
})
export class SimpleCardComponent extends ModalBaseCardComponent implements OnInit, AfterViewInit {

  @Input() userId = null;
  @Output() closeCard: EventEmitter<any> = new EventEmitter<any>();
  public selectedCity: any = null;
  cities = [
    { label: 'New York New York New York', value: 'NY' },
    { label: 'Rome', value: 'RM' },
    { label: 'London', value: 'LDN' },
    { label: 'IstanbulIstanbulIstanbulIstanbul 123London London ', value: 'IST' },
    { label: 'Paris', value: 'PRS' }
  ];
  val1 = 'Option 1';
  val2 = 'Option 22';
  date8 = new Date();
  selectedCategories: string[] = ['Technology', 'Sports'];
  selectedItem: any = null;
  currentIndex: any = null;
  showNewWindow: boolean;
  // bodyElement: ElementRef;

  // flags = {
  //   editingModeOff: true,
  // };
  i18nWords = ['SAVED_MSG', 'CLOSE_MSG'];
  displayDialogInfo: boolean;
  currentItem: any;

  constructor(
    protected elementRef: ElementRef,
    protected renderer: Renderer2,
    protected messageService: MessageService,
    protected translateService: TranslateService,
    private activateRoute: ActivatedRoute,
    private httpApiService: HttpApiService
  ) {
    super(elementRef, renderer, messageService, translateService);
    this.activateRoute.queryParamMap.pipe(
    ).subscribe((data) => {
      console.log(data)
      this.checkRouteLinkQueryParams(data)
    });
    this.activateRoute.paramMap.pipe(
    ).subscribe((params) => {
      console.log(params)
      this.checkRouteLinkParams(params)
    });
  }

  ngOnInit() {
    // console.log(this.userId)
    if (this.userId) {
      this.getUserData(this.userId)
    }
    this.getTranslatesRxJs(this.i18nWords).subscribe();
  }

  public checkRouteLinkQueryParams(params: ParamMap) {
    if (params.has('status') && params.get('status') === 'singlePage') {
      this.isStandalonePage = true;
      // this.activateRoute.params.subscribe(data => console.log(data))
    }
  }
  public checkRouteLinkParams(params: ParamMap) {
    if (this.isStandalonePage && params.has('id')) {
      this.userId = params.get('id');
    }
  }

  private getUserData(id = 0) {
    this.httpApiService.getSingleUser(id).subscribe(
      (data) => {
        if (data && 'data' in data) {
          this.currentItem = data.data;
        }
        console.log(data)
      },
      (error) => {
        console.log(error)
      }
    )
  }

  public focusOut(event) {
    console.log(event)
  }

  public dropDown(event) {
    console.log(event)
  }
  public openTab(event) {
    console.log(event)
  }

  openNewWindow() {
    this.showNewWindow = true;
  }
  saveCard() {
    super.saveCard()
    this.displayDialogInfo = true;
  }
}
