import { Component, OnInit, Renderer2, ElementRef, Inject } from '@angular/core';
import { BaseGridComponent } from '@share/base-components/base-grid/base-grid.component';
import { MessageService } from 'primeng/api';
import { HttpApiService } from '@share/services/http-api.service';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-example-table-page',
  templateUrl: './example-table-page.component.html',
  styleUrls: ['./example-table-page.component.less']
})
export class ExampleTablePageComponent extends BaseGridComponent implements OnInit {
  document: Document;
  // scrollHeightGrid = '500px';
  rows = 50;
  contextMenuVisible = true;
  constructor(
    protected messageService: MessageService,
    protected httpApiService: HttpApiService,
    protected renderer: Renderer2,
    protected elementRef: ElementRef,
    protected router: Router,
    @Inject(DOCUMENT) document: Document
  ) {
    super(
      elementRef,
      renderer,
      messageService
    );
    this.document = document;
  }

  dataGrid = [
    {
      subjectApplication: 'Поручитель',
      sourceInformation: 'Реєстр НБУ',
      status: 'Відкритий',
      type: 'НВКЛ',
      product: 'Придбання основних засобів/Капітальні інвестиції',
      initialAmount: '11 000 000',
      debtBalance: '9 645 000',
      currency: 'UAH',
      dateContract: '01.03.2019',
      repaymentDate: ' - /01.03.2021',
      paymentAmount: '18 600',
      underwriting: '18 600',
      dpd: '93 / 47 800',
      delayedCash: 'Так',
      repayCondition: 'V'
    },
    {
      subjectApplication: 'Чоловік / Дружина (Клієнт)',
      sourceInformation: 'ПВБКІ',
      status: 'Закритий',
      type: 'ВКЛ',
      product: 'Кредитна картка',
      initialAmount: '400 000',
      debtBalance: '0',
      currency: 'UAH',
      dateContract: '17.03.2019',
      repaymentDate: '15.03.2020 / 17.03.2020',
      paymentAmount: '2 600',
      underwriting: '2 600',
      dpd: '0 / 0',
      delayedCash: 'Ні',
      repayCondition: null
    },
    {
      subjectApplication: 'Чоловік / Дружина (Поручитель)',
      sourceInformation: 'FlexCube',
      status: 'Відкритий',
      type: 'НВКЛ',
      product: 'Придбання житлової нерухомості (квартира, будинок)',
      initialAmount: '65 000',
      debtBalance: '34 000',
      currency: 'USD',
      dateContract: '14.05.2014',
      repaymentDate: ' - / 13.05.2024',
      paymentAmount: '13 800',
      underwriting: '13 800',
      dpd: '181 / 112 000',
      delayedCash: 'Так',
      repayCondition: 'V'
    }
  ];

  ngOnInit() {
    this.createCols();
  }

  ngAfterViewInit() {

  }

  public createCols() {
    this.cols = [
      { field: 'subjectApplication', header: 'Суб\'єкт заявки', width: '100px' },
      { field: 'sourceInformation', header: 'Джерело інформації', width: '200px' },
      { field: 'status', header: 'Статус', width: '100px' },
      { field: 'type', header: 'Тип (ВКЛ/НВКЛ)', width: '100px' },
      { field: 'product', header: 'Продукт', width: '300px' },
      { field: 'initialAmount', header: 'Початкова сума', width: '100px' },
      { field: 'debtBalance', header: 'Залишок заборгованості', width: '100px' },
      { field: 'currency', header: 'Валюта', width: '100px' },
      { field: 'dateContract', header: 'Дата договору', width: '100px' },
      { field: 'repaymentDate', header: 'Дата погашення (план/факт)', width: '100px' },
      { field: 'paymentAmount', header: 'Сума платежу (UAH)', width: '100px' },
      { field: 'underwriting', header: 'Сума платежу (андеррайтинг)', width: '100px' },
      { field: 'dpd', header: 'Поточна прострочка (DPD/сума, UAH)', width: '100px' },
      { field: 'delayedCash', header: 'Наявна прострочка?', width: '100px' },
      { field: 'repayCondition', header: 'Умова Погашення', width: '100px' },
    ];
  }

  // private setTableBodyHeight(element, height = 167) {
  //   // const tableBody = this.elementRef.nativeElement.querySelector('.ui-table-scrollable-body');
  //   const windowHight = window.innerHeight;
  //   this.scrollHeightGrid = `${windowHight - height}px`;
  //   this.renderer.setStyle(element, 'height', this.scrollHeightGrid);
  // }

  // public getTableBody(element) {
  //   const tableBody = element.nativeElement.querySelector('.ui-table-scrollable-body');
  //   this.setTableBodyHeight(tableBody);
  // }

}
