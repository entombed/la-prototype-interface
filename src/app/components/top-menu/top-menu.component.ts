import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.less']
})
export class TopMenuComponent implements OnInit {
  public menuItems: MenuItem[];
  document: Document;

  constructor(
    private router: Router,
    @Inject(DOCUMENT) document: Document
  ) {
    this.document = document;
  }

  ngOnInit() {
    this.menuItems = [
      {
        label: 'Reesters',
        // command: (data) => { this.goto('Reester'); }
        command: (data) => { this.router.navigate([`/Reester`]); }
      },
      { separator: true },
      {
        label: 'Operations',
        // command: (data) => { this.goto('Operations'); }
        command: (data) => { this.router.navigate([`/Operations`]); }
      },
      { separator: true },
      {
        label: 'Dictionry',
        command: (data) => { this.goto(data); },
        disabled: true
      },
      {
        label: 'New Session',
        command: () => { this.createNewWindow(); },
      }
    ];
  }

  public goto(data) {
    this.router.navigate([`/${data}`]);
  }

  public createNewWindow() {
    window.open(`${document.location.protocol}//${this.document.location.host}/Operations`, '_blank');
  }

}
