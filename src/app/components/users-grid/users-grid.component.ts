import { Component, OnInit, Renderer2, ElementRef, Inject } from '@angular/core';
import { MessageService } from 'primeng/api';
import { HttpApiService } from '@share/services/http-api.service';
import { BaseGridComponent } from '@share/base-components/base-grid/base-grid.component';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-users-grid',
  templateUrl: './users-grid.component.html',
  styleUrls: ['./users-grid.component.less']
})
export class UsersGridComponent extends BaseGridComponent implements OnInit {
  showSimpleCard: boolean;
  // gridMenu = [
  //   { label: 'Open', icon: 'pi pi-search', command: (event) => this.openInNewTab(this.selectedItem) },
  // ];
  document: Document;
  selectedCardsId: any;
  standartHeight = false;
  constructor(
    protected messageService: MessageService,
    protected httpApiService: HttpApiService,
    protected renderer: Renderer2,
    protected elementRef: ElementRef,
    protected router: Router,
    @Inject(DOCUMENT) document: Document
  ) {
    super(
      elementRef,
      renderer,
      messageService
      );
    this.document = document;
  }

  ngOnInit() {
    this.createCols();
    this.getData();
  }
  ngAfterViewInit() {
    this.setTableBodyHeight('100px')
  }

  openInNewTab(data) {
    window.open(`${document.location.protocol}//${this.document.location.host}/simple/${data.id}?status=singlePage`, '_blank');
  }

  public createCols() {
    this.cols = [
      { field: 'email', header: 'EMAIL' },
      { field: 'first_name', header: 'FIRST_NAME' },
      { field: 'last_name', header: 'LAST_NAME' },
      { field: 'id', header: 'ID' },
    ];
  }

  private getData() {
    this.httpApiService.getUsersPage().subscribe(
      (data) => {
        this.dataGrid = data.data;
        // this.showInfoMsg('Loading Complite')
        // this.showErrorMsg('Kernel Panik!!!!')
      },
      (error) => { console.log(error); }
    );
  }

  public reload() {
    this.getData();
  }

  closeCard(event) {
    this.showSimpleCard = false;
    this.selectedCardsId = this.selectedItem.id;
  }

  handleRowDblclick(data) {
    this.showSimpleCard = true;
    console.log(data)
    console.log(this.selectedCards)
  }


}
