import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';
import { MessageService } from 'primeng/api';
import { HttpApiService } from '@share/services/http-api.service';
import { BaseGridComponent } from '@share/base-components/base-grid/base-grid.component';
import { switchMap } from 'rxjs/operators';
import { of, merge, forkJoin } from 'rxjs';

@Component({
  selector: 'app-unknown-grid',
  templateUrl: './unknown-grid.component.html',
  styleUrls: ['./unknown-grid.component.less']
})
export class UnknownGridComponent extends BaseGridComponent implements OnInit {

  public displayDialog = false;
  public displayDialog2 = false;
  public selectedCity: any = null;
  constructor(
    protected messageService: MessageService,
    protected httpApiService: HttpApiService,
    protected renderer: Renderer2,
    protected elementRef: ElementRef
  ) {
    super(elementRef, renderer, messageService);
  }

  ngOnInit() {
    this.createCols();
    this.getData();
  }

  public createCols() {
    this.cols = [
      { field: 'name', header: 'NAME' },
      { field: 'year', header: 'YEAR' },
      { field: 'pantone_value', header: 'PANTONE VALUE' },
      { field: 'color', header: 'COLOR' },
      { field: 'id', header: 'ID' },
    ];
  }

  private getData() {
    this.httpApiService.getUnknown(1).subscribe((data) =>  {
      // this.showInfoMsg('Loading Complite');
      this.dataGrid = data.data;
    })
  }

  public reload() {
    this.getData();
  }

  public showDialog() {
    this.displayDialog = true;
  }
  public showDialog2() {
    this.displayDialog2 = true;
  }
}
