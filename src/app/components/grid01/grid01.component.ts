import { Component, OnInit } from '@angular/core';
import { BaseCardComponent, CardFlags } from '@share/base-components/base-card/base-card.component';
import { CanComponentDeactivate } from '@share/guards/leave.guard';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-grid01',
  templateUrl: './grid01.component.html',
  styleUrls: ['./grid01.component.less']
})
export class Grid01Component implements OnInit, CanComponentDeactivate {

  public flags: CardFlags = {
    editingModeOff: true, // карточка в режиме редактирования или нет
  };
  public additive = '';
  public selectedCity = '';

  dataChart = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'My First dataset',
        backgroundColor: '#42A5F5',
        borderColor: '#1E88E5',
        data: [65, 59, 80, 81, 56, 55, 40]
      },
      {
        label: 'My Second dataset',
        backgroundColor: '#9CCC65',
        borderColor: '#7CB342',
        data: [28, 48, 40, 19, 86, 27, 90]
      }
    ]
  };

  dataPolar = {
    datasets: [{
      data: [
        11,
        16,
        7,
        3,
        14
      ],
      backgroundColor: [
        '#FF6384',
        '#4BC0C0',
        '#FFCE56',
        '#E7E9ED',
        '#36A2EB'
      ],
      label: 'My dataset'
    }],
    labels: [
      'Red',
      'Green',
      'Yellow',
      'Grey',
      'Blue'
    ]
  };

  cities = [
    { name: 'New York New York New York', code: 'NY' },
    { name: 'Rome', code: 'RM' },
    { name: 'London', code: 'LDN' },
    { name: 'IstanbulIstanbulIstanbulIstanbul 123London London ', code: 'IST' },
    { name: 'Paris', code: 'PRS' }
  ];
  val1 = 'Option 1';
  val2 = 'Option 22';
  date8 = new Date();
  selectedCategories: string[] = ['Technology', 'Sports'];
  data: any[] = [
    { LiqOrder: 1, Status: 'OVE', CompCode: 'ADD_INT', CompDesc: 'Дополнительный процент' },
    { LiqOrder: 2, Status: 'OVE', CompCode: 'MAIN_INT', CompDesc: 'Задолженность по начисленным доходам' },
    { LiqOrder: 3, Status: 'OVE', CompCode: 'PRINCIPAL', CompDesc: 'Задолженность по телу' },
    { LiqOrder: 4, Status: 'NORM', CompCode: 'ADD_INT', CompDesc: 'Дополнительный процент' },
    { LiqOrder: 5, Status: 'NORM', CompCode: 'MAIN_INT', CompDesc: 'Задолженность по начисленным доходам' },
    { LiqOrder: 6, Status: 'NORM', CompCode: 'PRINCIPAL', CompDesc: 'Задолженность по телу' }
  ];
  cols: any[] = [
    { field: 'LiqOrder', header: 'ORDER' },
    { field: 'Status', header: 'STATUS' },
    { field: 'CompCode', header: 'CODE' },
    { field: 'CompDesc', header: 'DESCRIPTION' },
  ];
  selectedItem: any = null;
  currentIndex: any = null;
  showSimpleCard = false;
  constructor() { }

  ngOnInit() {
  }

  rowSelect(rowData) {
    console.log(rowData);
    this.currentIndex = rowData.index;
    console.log(this.currentIndex);
  }

  action(action) {
    let i = 0;
    let nextIndex: number = null;
    let curData: any[] = null;
    let nextData: any[] = null;
    if (action === 'down') {
      i = 1;
    } else if (action === 'up') {
      i = -1;
    }
    nextIndex = this.currentIndex + i;
    if (this.selectedItem && nextIndex !== -1 && nextIndex < this.data.length) {
      curData = [this.data[this.currentIndex]];
      nextData = [this.data[nextIndex]];
      this.data[this.currentIndex] = nextData[0];
      this.data[nextIndex] = curData[0];
      this.currentIndex = nextIndex;
      this.data.forEach((item, index) => {
        item.LiqOrder = index + 1;
      });
    }
  }

  save() {
    console.log(this.data);
  }

  closeCard(event) {
    this.showSimpleCard = false;
  }

  focusOut(data) {}
  dropDown(data) {}

  canDeactivate(): boolean | Observable<boolean> {
    return confirm('Вы хотите покинуть страницу?');
  }

}
